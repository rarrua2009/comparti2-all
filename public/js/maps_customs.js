let latitude;
let longitude;
// let precio_viaje = $('meta[name="appUrl"]').attr('content');
let precio_viaje = 300;
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function getBeginAddress(map,marker){

    var infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
            marker.setPosition(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    const locationInputs = document.getElementsByClassName("map-input");
    const autocompletes = [];
    const geocoder = new google.maps.Geocoder;

    for (let i = 0; i < locationInputs.length; i++) {

        const input = locationInputs[i];
        const fieldKey = input.id.replace("-input", "");

        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.key = fieldKey;
        autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});
    }

    for (let i = 0; i < autocompletes.length; i++) {
        const input = autocompletes[i].input;
        const autocomplete = autocompletes[i].autocomplete;
        const map = autocompletes[i].map;
        const marker = autocompletes[i].marker;

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            marker.setVisible(false);
            const place = autocomplete.getPlace();

            geocoder.geocode({'placeId': place.place_id}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    const lat = results[0].geometry.location.lat();
                    const lng = results[0].geometry.location.lng();
                    setLocationCoordinates(autocomplete.key, lat, lng);
                }
            });

            if (!place.geometry) {
                window.alert("No hay detalles disponibles para dicho lugar: '" + place.name + "'");
                input.value = "";
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(20);
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

        });
    }
return marker;

}//end beginAddress

function getEndAddress(map,marker2) {

    const endInputs = document.getElementsByClassName("end-input");
    const autocompletes_end = [];
    const geocoder = new google.maps.Geocoder;

    for (let i = 0; i < endInputs.length; i++) {

        const input = endInputs[i];
        const fieldKey = input.id.replace("-input", "");

        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.key = fieldKey;
        autocompletes_end.push({input: input, map: map, marker: marker2, autocomplete: autocomplete});
    }



    for (let i = 0; i < autocompletes_end.length; i++) {
        const input = autocompletes_end[i].input;
        const autocomplete = autocompletes_end[i].autocomplete;
        const map = autocompletes_end[i].map;
        const marker = autocompletes_end[i].marker;

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            marker2.setVisible(false);
            const place = autocomplete.getPlace();

            geocoder.geocode({'placeId': place.place_id}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    const lat = results[0].geometry.location.lat();
                    const lng = results[0].geometry.location.lng();
                    setLocationCoordinatesEnd(autocomplete.key, lat, lng);
                    calculePriceBeginEnd();
                }
            });

            if (!place.geometry) {
                window.alert("No hay detalles disponibles para dicho lugar: '" + place.name + "'");
                input.value = "";
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(20);
            }
            marker2.setPosition(place.geometry.location);
            marker2.setVisible(true);

        });
    }

 return marker2;
}//end endAddres

function getStopAddress(map,marker3) {

    const stopInputs = document.getElementsByClassName("stop-input");
    const autocompletes_stop = [];
    const geocoder = new google.maps.Geocoder;

    for (let i = 0; i < stopInputs.length; i++) {

        const input = stopInputs[i];
        const fieldKey = input.id.replace("-input", "");

        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.key = fieldKey;
        autocompletes_stop.push({input: input, map: map, marker: marker3, autocomplete: autocomplete});
    }

    for (let i = 0; i < autocompletes_stop.length; i++) {
        const input = autocompletes_stop[i].input;
        const autocomplete = autocompletes_stop[i].autocomplete;
        const map = autocompletes_stop[i].map;
        const marker = autocompletes_stop[i].marker;

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            marker3.setVisible(false);
            const place = autocomplete.getPlace();

            geocoder.geocode({'placeId': place.place_id}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    const lat = results[0].geometry.location.lat();
                    const lng = results[0].geometry.location.lng();
                    setLocationCoordinatesStop(autocomplete.key, lat, lng);
                    calculatePriceBeginStop();
                }
            });

            if (!place.geometry) {
                window.alert("No hay detalles disponibles para dicho lugar: '" + place.name + "'");
                input.value = "";
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(20);
            }
            marker3.setPosition(place.geometry.location);
            marker3.setVisible(true);

        });
    }

    return marker3;
}//end StopAddress

function initialize() {
    $('form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    const map = new google.maps.Map(document.getElementById('address-map'), {
        center: {lat: -33.8688, lng: 151.2195},
        zoom: 13,
        gestureHandling: 'cooperative'
    });

    var marker2 = new google.maps.Marker({
        map: map,
        position: {lat: -33.8688, lng: 151.2195},
    });

    var marker3 = new google.maps.Marker({
        map: map,
        position: {lat: -33.8688, lng: 151.2195},
    });

    var marker = new google.maps.Marker({
        map: map,
        position: {lat: -33.8688, lng: 151.2195},
    });

    getBeginAddress(map,marker);
    getEndAddress(map,marker2);
    getStopAddress(map,marker3);
}

function setLocationCoordinates(key, lat, lng) {
    const latitudeField = document.getElementById("address-latitude");
    const longitudeField = document.getElementById("address-longitude");
    latitudeField.value = lat;
    longitudeField.value = lng;
}

function setLocationCoordinatesEnd(key, lat, lng) {
    const latitudeField = document.getElementById("end_addres-latitude");
    const longitudeField = document.getElementById("end_addres-longitude");
    latitudeField.value = lat;
    longitudeField.value = lng;
}

function setLocationCoordinatesStop(key, lat, lng) {
    const latitudeField = document.getElementById("stop-latitude");
    const longitudeField = document.getElementById("stop-longitude");
    latitudeField.value = lat;
    longitudeField.value = lng;
}

function calculePriceBeginEnd() {
    //$('#price').val();
    const latitudeField = $("#address-latitude");
    const longitudeField = $("#address-longitude");

    const latitudeField_end = $("#end_addres-latitude");
    const longitudeField_end = $("#end_addres-longitude");

    console.log("Lat - lng begin -->" + latitudeField.val() + " " +longitudeField.val() );
    console.log("Lat - lng end -->" + latitudeField_end.val() + " " +latitudeField_end.val() );

    var a = new google.maps.LatLng(latitudeField.val(),longitudeField.val());
    var b = new google.maps.LatLng(latitudeField_end.val(),longitudeField_end.val());
    var distance = (google.maps.geometry.spherical.computeDistanceBetween(a,b)/1000).toFixed(0);

    console.log("begin end price -->" + distance);
    var precio = distance * precio_viaje;
    //precio = distance * precio_viaje;
    //precio.format('0.0');
    console.log("El precio ---->"+precio);

    $('#price').val('Gs. ' + $.number(precio,0, '', '.' ));
}

function calculatePriceBeginStop() {

    const latitudeField = $("#address-latitude");
    const longitudeField = $("#address-longitude");

    const latitudeField_stop = $("#stop-latitude");
    const longitudeField_stop = $("#stop-longitude");

    var a = new google.maps.LatLng(latitudeField.val(),longitudeField.val());
    var b = new google.maps.LatLng(latitudeField_stop.val(),longitudeField_stop.val());

    var distance = (google.maps.geometry.spherical.computeDistanceBetween(a,b)/1000).toFixed(0);
    console.log("begin - stop -->" + distance);
    var precio = numeral();
    precio = distance * precio_viaje;
    precio.format('0.0');
    $('#price_to_stop').val(precio);
}
