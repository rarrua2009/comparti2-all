<?php

use Illuminate\Database\Seeder;
use App\Models\Admins;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [[
            'name'           => 'Admin',
            'email'          => 'admin@admin.com',
            'password'       => Hash::make('123456'),
            'remember_token' => null,
            'created_at'     => '2019-04-15 19:13:32',
            'updated_at'     => '2019-04-15 19:13:32',
        ]];

        Admins::insert($admin);
    }
}
