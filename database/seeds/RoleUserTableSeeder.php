<?php

use App\Models\Admins;
use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    public function run()
    {
        //User::findOrFail(1)->roles()->sync(1);
        Admins::findOrFail(1)->roles()->sync(1);
    }
}
