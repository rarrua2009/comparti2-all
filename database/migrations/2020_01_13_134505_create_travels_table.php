<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;

class CreateTravelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('begin_location_lat','10','8');
            $table->decimal('begin_location_long','11','8');
            $table->decimal('end_location_lat','10','8');
            $table->decimal('end_location_long','11','8');
            $table->decimal('stop_location_lat','11','8')->nullable();
            $table->decimal('stop_location_long','11','8')->nullable();
            $table->string('begin_address');
            $table->string('end_address');
            $table->string('stop_address')->nullable();
            $table->boolean('round trip')->default(false);
            $table->dateTime('departure_date_time');
            $table->unsignedInteger('user_id');
            $table->unsignedBigInteger('path_id');
            $table->enum('state',['active','reserved','in-progress','canceled','ended'])->default('active');
            $table->integer('price');
            $table->integer('price_to_stop')->nullable();
            $table->smallInteger('seat')->default(1);
            $table->unsignedBigInteger('car_id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('path_id')->references('id')->on('paths');
            $table->foreign('car_id')->references('id')->on('cars');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travels');
    }
}
