<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Passengers
 *
 * @property int $id
 * @property int $user_id
 * @property int $travel_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Travels $travel
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Passengers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Passengers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Passengers query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Passengers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Passengers whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Passengers whereTravelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Passengers whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Passengers whereUserId($value)
 */
	class Passengers extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Cars
 *
 * @property int $id
 * @property string $name
 * @property string $brand
 * @property string $model
 * @property string $year
 * @property int $active
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $license_front
 * @property string $plate
 * @property string $license_back
 * @property string $green_license_front
 * @property string $green_license_back
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Travels[] $travels
 * @property-read int|null $travels_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereGreenLicenseBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereGreenLicenseFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereLicenseBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereLicenseFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars wherePlate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cars whereYear($value)
 */
	class Cars extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Travels
 *
 * @property int $id
 * @property float $begin_location_lat
 * @property float $begin_location_long
 * @property float $end_location_lat
 * @property float $end_location_long
 * @property string $begin_address
 * @property string $end_address
 * @property \Illuminate\Support\Carbon $departure_date_time
 * @property int $user_id
 * @property int $path_id
 * @property string $state
 * @property int $price
 * @property int $car_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $stop_address
 * @property float|null $stop_lat
 * @property float|null $stop_long
 * @property int|null $price_to_stop
 * @property int $seat
 * @property-read \App\Models\Cars $car
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Passengers[] $passengers
 * @property-read int|null $passengers_count
 * @property-read \App\Models\Paths $paths
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereBeginAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereBeginLocationLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereBeginLocationLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereDepartureDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereEndAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereEndLocationLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereEndLocationLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels wherePathId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels wherePriceToStop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereSeat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereStopAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereStopLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereStopLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travels whereUserId($value)
 */
	class Travels extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Paths
 *
 * @property int $id
 * @property string $name
 * @property string $origin
 * @property string $destination
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $round_trip
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Suscriptions[] $suscriptions
 * @property-read int|null $suscriptions_count
 * @property-read \App\Models\Travels $travel
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths whereDestination($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths whereOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths whereRoundTrip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Paths whereUpdatedAt($value)
 */
	class Paths extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string|null $phone
 * @property \Illuminate\Support\Carbon|null $date_birth
 * @property string $sex
 * @property string $type
 * @property int $state
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cars[] $cars
 * @property-read int|null $cars_count
 * @property-read mixed $full_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Notifications[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Suscriptions[] $suscriptions
 * @property-read int|null $suscriptions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Travels[] $travels
 * @property-read int|null $travels_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDateBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string|null $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Role withoutTrashed()
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notifications
 *
 * @property int $id
 * @property string $content
 * @property int $user_id
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notifications newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notifications newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notifications query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notifications whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notifications whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notifications whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notifications whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notifications whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notifications whereUserId($value)
 */
	class Notifications extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Suscriptions
 *
 * @property int $id
 * @property int $path_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Paths $path
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Suscriptions newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Suscriptions newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Suscriptions query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Suscriptions whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Suscriptions whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Suscriptions wherePathId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Suscriptions whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Suscriptions whereUserId($value)
 */
	class Suscriptions extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Admins
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property mixed $email_verified_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admins whereUpdatedAt($value)
 */
	class Admins extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string|null $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

