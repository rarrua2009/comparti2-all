<div class="sidebar">
    <nav class="sidebar-nav ps ps--active-y">

        <ul class="nav">
            <li class="nav-item">
                <a href="{{ route("admin.home") }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt">
                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            <li class="nav-title">Menu</li>
            <li class="nav-item nav-dropdown">
                <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                    <i class="fas fa-users nav-icon">
                    </i>
                    Usuarios
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                            <i class="fas fa-user nav-icon">
                            </i>
                            {{ trans('global.user.title') }}
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle">
                    <i class="fas fa-luggage-cart nav-icon">
                    </i>
                    Viajes
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route("admin.home") }}" class="nav-link">
                            <i class="fas fa-list nav-icon">
                            </i>
                            Lista
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route("admin.home") }}" class="nav-link">
                    <i class="nav-icon fas fa-bell"></i>
                    Notificaciones
                </a>
            </li>

{{--            <li class="nav-item">--}}
{{--                <a href="{{ route("admin.products.index") }}" class="nav-link {{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}">--}}
{{--                    <i class="fas fa-boxes nav-icon">--}}

{{--                    </i>--}}
{{--                    {{ trans('global.product.title') }}--}}
{{--                </a>--}}
{{--            </li>--}}
        </ul>

        <div class="ps__rail-x" style="left: 0; bottom: 0;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0; width: 0;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0; height: 869px; right: 0;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0; height: 415px;"></div>
        </div>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
