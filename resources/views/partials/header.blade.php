<header class="app-header navbar">

    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand mt-xl-0 mt-lg-0 mt-md-0 mt-sm-0 mt-xs-0 pb-xl-2 pd-sm-1 pb-md-1 pb-lg-2 mr-xl-4 mr-lg-3 ml-xl-3 justify-content-center">
        <img src="{{ asset('img/logo.png')}}" class="ml-xl-2 ml-lg-2" alt="Portuaria" width="100">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none ml-sm-0 pl-sm-0 pl-md-0 ml-md-0" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a aria-expanded="false" aria-haspopup="true" class="nav-link" data-toggle="dropdown" href="#" role="button">
                <img alt="admin@admin.com" class="img-avatar" src="{{ asset('img/avatar/avatar.jpg') }}">
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>
                        Configuración
                    </strong>
                </div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-user">
                    </i>
                    Perfil
                </a>
                <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-sign-out-alt">
                    </i>
                    {{ trans('global.logout') }}
                </a>
            </div>
        </li>
    </ul>
{{--    <button class="navbar-toggler aside-menu-toggler d-md-down-none" data-toggle="aside-menu-lg-show" type="button">--}}
{{--        <span class="navbar-toggler-icon">--}}
{{--        </span>--}}
{{--    </button>--}}
{{--    <button class="navbar-toggler aside-menu-toggler d-lg-none" data-toggle="aside-menu-show" type="button">--}}
{{--        <span class="navbar-toggler-icon">--}}
{{--        </span>--}}
{{--    </button>--}}

</header>
