@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            Ajustes del sistema
        </div>

        <div class="card-body">
            <form action="{{ route("settings.update", [$setting->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    <label for="precio_por_viaje">Precio por viaje</label>
                    <input type="text" id="precio_por_viaje" name="precio_por_viaje" class="form-control"
                           value="{{ old('precio_por_viaje', isset($setting) ? $setting->precio_por_viaje : '') }}">
                    @if($errors->has('precio_por_viaje'))
                        <em class="invalid-feedback">
                            {{ $errors->first('precio_por_viaje') }}
                        </em>
                    @endif

                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                </div>
            </form>
        </div>
    </div>

@endsection<?php
