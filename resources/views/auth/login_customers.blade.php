@extends('layouts.auth')
@section('content')

    <!-- formulario -->

    <section class="showcase bg-light ">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row group">
                <figure class="col-md-3 col-lg-3 col-sm-12 espacios" style="margin-bottom: 0px !important;">
                    <img src="{{ asset('img/registrar.jpg') }}" class="box">
                </figure>
                <div class="col-md-9 col-lg-9  col-sm-12 espacios" >


                    <form class=" container-fluid mt-5 col-md-9 col-lg-9  col-sm-12 espacios" action="{{ route('page.customer.login.post') }}" method="POST">
                        @csrf
                        <div class="col-md-9 col-lg-9  col-sm-12">
                            <div class="col-lg-9">
                                <h3>Inicia Sesión</h3>
                                <small class="form-text text-muted">Si ya tienes una cuenta? Inicia sesión para publicar o unirte a un viaje compartido</small>
                                <div class="form-group mt-5">
                                    <input type="email" class="form-control borderiz borderder" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Correo electrónico">

                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control borderiz borderder" name="password" id="exampleInputPassword1" placeholder="Contraseña">

                                </div>
                                <button type="submit" class="btn btn-dark btn-block borderiz borderder">Iniciar Sesion</button>
                                <small class="form-text text-muted text-center mt-3">Aún no tienes una cuenta? <a href="{{ route('page.customer.register') }}" class="text-success">Regístrate</a></small>
                                <small class="form-text text-muted text-center mt-3">Olvido su contraseña? <a href="{{ route('password.request') }}" class="text-success">Click aqui</a></small>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection
