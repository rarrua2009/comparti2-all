@extends('layouts.auth')

@section('content')
    <section class="showcase bg-light ">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row group">
                <figure class="col-md-3 col-lg-3 col-sm-12 espacios" style="margin-bottom: 0px !important;">
                    <img src="{{ asset('img/registrar.jpg') }}" class="box">
                </figure>
                <div class="col-md-9 col-lg-9 col-sm-12 espacios" >

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class=" container-fluid mt-5 col-md-9 col-lg-9 col-sm-12 espacios" action="{{ route('page.customer.register.post') }}" method="POST">
                        @csrf
                        <div class="col-md-9 col-lg-9  col-sm-12">
                            <div class="col-lg-9">
                                <h3>Registrate</h3>
                                <small class="form-text text-muted">Aún no tienes una cuenta? Regístrate! Para publicar o unirte a un viaje compartido</small>
                                <div class="form-group mt-5">
                                    <input type="text" class="form-control borderiz borderder" placeholder="Nombres" name="firstname">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control borderiz borderder" placeholder="Apellidos" name="lastname">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control borderiz borderder" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Correo electrónico">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control borderiz borderder" name="phone" aria-describedby="emailHelp" placeholder="Número de telefono">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control borderiz borderder" id="date_birth" name="date_birth" placeholder="Fecha de nacimiento">
                                </div>
                                <div class="form-group">
                                    <select class="form-control borderiz borderder" name="type">
                                        <option value="passenger">Pasajero</option>
                                        <option value="traveler">Viajero</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="bg-light form-control borderiz borderder mb-3" name="sex">
                                        <option value="masculino" selected>Masculino</option>
                                        <option value="femenino">Femenino</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control borderiz borderder" id="exampleInputPassword1" name="password" placeholder="Contraseña">

                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control borderiz borderder" id="exampleInputPassword2" name="password_confirmation" placeholder="Repite la contraseña">

                                </div>
                                <button class="btn btn-dark btn-block borderiz borderder" type="submit">Registrarse</button>
                                <small class="form-text text-muted text-center mt-3">Ya tienes una cuenta? <a href="{{ route('page.customer.login') }}" class="text-success">Inicia Sesión</a></small>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
@endsection

