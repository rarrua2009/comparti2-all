<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="appUrl" content="{{ url('page') }}">

    <meta name="description" content="Comparti2 - Otra manera de viajer y ahorrar dinero">
    <meta name="keywords" content="cars, share, travel, customers, money, two">
    <meta name="author" content="Nano developers">

    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/icons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/icons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/icons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/icons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/icons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/icons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/icons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/icons/favicon-16x16.png') }}">


    <!-- Web Application Manifest -->
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('images/icons/mstile-150x150.png') }}">
    <meta name="theme-color" content="#ffffff">

    <title>Comparti2</title>
    @laravelPWA
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/landing-page.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pickerjs/themes/default.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pickerjs/themes/default.date.css') }}" rel="stylesheet">
    <style type="text/css">
        .borderder{
            border-top-right-radius: 20px !important;
            border-bottom-right-radius: 20px !important;
        }


        .borderiz{
            border-top-left-radius: 20px !important;
            border-bottom-left-radius: 20px !important;
        }

        .box{
            width: 100%;
            height: 100%;
        }


        .espacios{
            padding: 0px !important;
        }

    </style>

</head>

<body >

<!-- Navigation -->
<div class="container mb-2 mt-2 ">
    <nav class="navbar navbar-expand-lg navbar-light text-center" >
        <a title="logo" href="{{ route('page.landing') }}">
            <img  class="navbar-brand" src="{{ asset('img/logo.png') }}"alt="logo">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav ml-auto text-center">
                <a class="nav-item nav-link" href="#">Publicar Viaje <span class="sr-only">(menu)</span></a>
                <a class="nav-item nav-link" href="#">Buscar Viaje</a>
                <a class="nav-item nav-link text-success" href="{{ route('page.customer.login') }}">Iniciar Sesión</a>
                <a class="btn btn-success borderder borderiz " href="{{ route('page.customer.register') }}"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Registro&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
            </div>
        </div>
    </nav>
</div>

@yield('content')

<!-- Bootstrap - JavaScript -->
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>


<script src="{{ asset('vendor/pickerjs/legacy.js') }}"></script>
<script src="{{ asset('vendor/pickerjs/picker.js') }}"></script>
<script src="{{ asset('vendor/pickerjs/picker.date.js') }}"></script>
<script src="{{ asset('vendor/pickerjs/translations/es_ES.js') }}"></script>

<script type="text/javascript">
    // Your code goes here.
    $(document).ready(function () {
        $('#date_birth').pickadate({
            selectYears: 80,
            min:new Date(1940,0,1),
            max: new Date(2002,0,1),
            selectMonths: true,
            format: 'd mmmm, yyyy',
            formatSubmit: 'yyyy-mm-dd',
            hiddenPrefix: undefined,
            hiddenName: 'date_birth_submited',

        });
    })
</script>

</body>

</html>
