<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="appUrl" content="{{ url('page') }}">
    <meta name="precio_pasaje" content="{{ $precio_viaje }}">

    <meta name="description" content="Comparti2 - Otra manera de viajer y ahorrar dinero">
    <meta name="keywords" content="cars, share, travel, customers, money, two">
    <meta name="author" content="Nano developers">

    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/icons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/icons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/icons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/icons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/icons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/icons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/icons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/icons/favicon-16x16.png') }}">


    <!-- Web Application Manifest -->
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('images/icons/mstile-150x150.png') }}">
    <meta name="theme-color" content="#ffffff">

    <title>Comparti2</title>
    @laravelPWA
{{--    <link href="{{ asset('css/estilos-2.css') }}" rel="stylesheet">--}}
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">


    <!-- Custom styles for this template -->
    <link href="{{ asset('css/landing-page.min.css') }}" rel="stylesheet">

    @yield('style')
    @toastr_css
    <style type="text/css">

        .searchbar{
            margin-bottom: auto;
            margin-top: auto;
            height: 60px;
            background-color: #f8f9fa;
            border-radius: 30px;
            padding: 10px;
        }

        .search_input{
            color: black;
            border: 0;
            outline: 0;
            background: none;
            width: 0;
            caret-color:black;
            line-height: 40px;
            transition: width 0.4s linear;
        }

        .searchbar:hover > .search_input{
            padding: 0 10px;
            width: 750px;
            caret-color:white;
            transition: width 0.4s linear;
        }



        .search_icon{
            height: 40px;
            width: 40px;
            float: right;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            color:#5cd169;
            text-decoration:none;
        }



        .bordes {
            border-top-right-radius: 40px !important;
            border-bottom-right-radius: 40px !important;
            border-top-left-radius: 40px !important;
            border-bottom-left-radius: 40px !important;
        }

        .borderder{
            border-top-right-radius: 20px !important;
            border-bottom-right-radius: 20px !important;
        }

        .borderiz{
            border-top-left-radius: 20px !important;
            border-bottom-left-radius: 20px !important;
        }

        .box{
            width: 100%;
            height: 100%;
        }

        .espacios{
            padding: 0px !important;
        }


        #container2 {
            width: auto;
            height: auto;
            overflow: hidden;
            padding-bottom: 20px;
        }
        #modules::-webkit-scrollbar {
            height: 5px;
            /*background: #303030;*/
            background: white;
        }


        #modules::-webkit-scrollbar:hover {
            height: 5px;
            background: #5cd169;
        }

        #modules {
            height:auto;
            white-space:nowrap;
            overflow-x: scroll;
            overflow-y: hide;
            -webkit-overflow-scrolling:touch;

            &::-webkit-scrollbar {
                 display: none;
            }


        }

        .module {
            display:inline-block;
            width:auto;
            height:auto;
            line-height:auto;
            text-align:center;
        }
        .module + .module {
            margin-left:5px
        }


        @media (min-width: 360px) {
            .2{
                padding: 0px;

            }

            /*.searchbar:hover > .search_input{*/
            /*    width: 50%;*/
            /*}*/
        }

        @media (max-width: 760px) {
            .searchbar:hover > .search_input{
                width: 50%;
            }

        }

    </style>

</head>

<body>
    @include('layouts.frontend.header')
        @yield('content')
    @include('layouts.frontend.footer')

    <!-- Bootstrap - JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>


    <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

    <script>
        $(function() {
            toastr.options.closeButton = true;
            toastr.options.closeMethod = 'fadeOut';
            toastr.options.closeDuration = 200;
        });
    </script>
    @yield('scripts')
    @toastr_js
    @toastr_render
</body>
</html>
