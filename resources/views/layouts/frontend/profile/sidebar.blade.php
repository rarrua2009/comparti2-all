<!-- Sidebar -->
<div class=" border-right pl-5" id="sidebar-wrapper" style="width: 300px">
    <div class=" foto">
        <div class="borderder borderiz bg-gray1 margen mt-5 mb-4" style= "height: 100px; width: 100px;">
            <img src="{{ asset('img/avatar/avatar.jpg') }}" style="width: 100%;  height: 100px;" class="borderder borderiz mx-auto img-fluid d-block" alt="avatar">
        </div>

        <div class="sidebar-heading">
            <h4>{{ Auth::user()->fullName }} </h4>
            <h6 class="text-muted mb-4 font-weight-light">{{ Auth::user()->email }}</h6>
        </div>
    </div>

    <div class="list-group list-group-flush menu pr-4">
        <a href="{{ route('page.profile.index', Auth::id()) }}" class="mb-2 text-left {{ request()->is('page/profile/account/*') ? 'active' : '' }} btn btn-success4 borderiz borderder">
            <i class="far fa-user mr-3 text-success"></i>
            Mi cuenta
        </a>
        @if(Auth::user()->type == 'passenger')
            <a href="{{ route('page.profile.travels.reserved') }}" class=" mb-2 text-left {{ request()->is('page/profile/travels/reserved/') || request()->is('page/profile/travels/reserved/*') ? 'active' : '' }}  btn btn-success4 borderiz borderder">
                <i class="far fa-star mr-3 text-success"></i>
                Viajes reservados <!-- de la tabla passengers, si su user_id esta alli es porque reservo-->
            </a>
            <a href="#" class=" mb-2 text-left {{ request()->is('page/profile/reviews') || request()->is('page/profile/reviews/*') ? 'active' : '' }} btn btn-success4 borderiz borderder">
                <i class="fas fa-award mr-3 text-success"></i>
                Reviews
            </a>
            <a href="{{ route('page.profile.payments') }}" class=" mb-2 text-left {{ request()->is('profile/payments/') || request()->is('profile/payments/*') ? 'active' : '' }}  btn btn-success4 borderiz borderder">
                <i class="fas fa-briefcase mr-3 text-success"></i>

                Pagos
            </a>
            <a href="{{ route('page.profile.notifications') }}" class=" mb-2 text-left {{ request()->is('page/profile/notifications') || request()->is('page/profile/notifications/*') ? 'active' : '' }} btn btn-success4 borderiz borderder">
                <i class="far fa-envelope mr-3 text-success"></i>
                Notificaciones
            </a>

        @endif
        @if(Auth::user()->type == 'traveler')
            <a href="{{ route('page.profile.travels.created') }}" class=" mb-2 text-left {{ request()->is('page/profile/travels/created') || request()->is('page/profile/travels/created/*') ? 'active' : '' }} btn btn-success4 borderiz borderder">
                <i class="fas fa-tag mr-3 text-success"></i>
                Viajes publicados <!-- de la tabla travels, si su user_id esta alli es porque publico -->
            </a>

            <a href="{{ route('page.profile.cars.index') }}" class=" mb-2 text-left {{ request()->is('page/profile/cars') || request()->is('page/profile/cars/*') ? 'active' : '' }} btn btn-success4 borderiz borderder">
                <i class="fas fa-car mr-3 text-success"></i>

                Autos
            </a>

            <a href="#" class=" mb-2 text-left {{ request()->is('profile/') || request()->is('profile/*') ? 'active' : '' }} btn btn-success4 borderiz borderder">
                <i class="fas fa-dollar-sign mr-3 text-success"></i>

                Transferencia
            </a>
            <a href="{{route('page.profile.notifications')}}" class=" mb-2 text-left {{ request()->is('page/profile/notifications') || request()->is('page/profile/notifications/*') ? 'active' : '' }} btn btn-success4 borderiz borderder">
                <i class="far fa-envelope mr-3 text-success"></i>
                Notificaciones
            </a>
        @endif


    </div>
</div>

<!-- /#sidebar-wrapper -->
