@extends('layouts.frontend.master')
@section('content')
    <div class="grid" id="wrapper">
        @include('layouts.frontend.profile.sidebar')
        @yield('page-content')
    </div>
@endsection
