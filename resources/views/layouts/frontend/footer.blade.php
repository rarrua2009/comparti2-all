<!-- Footer -->
<footer class="footer bg-light">
    <div class="container text-center">
        <img  href="index.html" src="{{ asset('img/logo.png') }}" alt="logo">

    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
                <p class="text-muted small mb-4 mb-lg-0">&copy; Comparti2. Todos los derechos reservados</p>
            </div>
            <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
                <p class="text-muted small mb-4 mb-lg-0">Power by @NANO Deverloper</p>
            </div>
        </div>
    </div>
</footer>
