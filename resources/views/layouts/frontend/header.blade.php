<!-- Navigation -->
<div class="container mb-2 mt-2 ">
    <nav class="navbar navbar-expand-lg navbar-light text-center" >
        <a title="logo" href="{{ route('page.landing') }}">
            <img  class="navbar-brand" src="{{ asset('img/logo.png') }}" alt="logo">
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav ml-auto text-center">
                @guest()
                <a class="nav-item nav-link text-success2" href="{{ route('page.travel.create') }}"><i class="fas fa-plus-circle"></i>
                    Publicar Viaje <span class="sr-only">(menu)</span></a>
                <a class="nav-item nav-link text-success2" href="{{ route('page.travel.find') }}"> <i class="fas fa-search"></i> Buscar Viaje</a>
                <a class="nav-item nav-link text-success" href="{{ route('page.customer.login') }}">Iniciar Sesión</a>
                <a class="btn btn-success2 borderder borderiz " href="{{ route('page.customer.register') }}"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Registro&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                @endguest

                @if(Auth::check())

                   @if(Auth::user()->type == 'passenger')
                        <a class="nav-item nav-link" href="{{ route('page.travel.find') }} ">Buscar viaje</a>
                   @else
                        <!-- Alertas de notificaciones -->
                            <li class="nav-item dropdown no-arrow mx-1">
                                <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bell fa-fw"></i>
                                    <!-- Counter - Alerts -->
                                    <span class="badge badge-danger badge-counter">3+</span>
                                </a>
                                <!-- Dropdown - Alerts -->
                                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                                    <h6 class="dropdown-header">
                                        Centro de notificaciones
                                    </h6>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-primary">
                                                <i class="fas fa-file-alt text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">{{ \Carbon\Carbon::now()->calendar() }}</div>
                                            Su viaje a Encarnacion del dia Viernes 25 de Febrero fue reservado por 1 pasajero
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-success">
                                                <i class="fas fa-donate text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">{{ \Carbon\Carbon::now()->calendar() }}</div>
                                            Su viaje a Encarnacion del dia Viernes 25 de Febrero fue reservado por 1 pasajero
                                        </div>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center" href="#">
                                        <div class="mr-3">
                                            <div class="icon-circle bg-warning">
                                                <i class="fas fa-exclamation-triangle text-white"></i>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="small text-gray-500">{{ \Carbon\Carbon::now()->sub('8 days')->calendar() }}</div>
                                            Se ha acredito a tu cuenta GS. 320.000
                                        </div>
                                    </a>
                                    <a class="dropdown-item text-center small text-gray-500" href="#">Mostrar todas las alertas</a>
                                </div>
                            </li>
                            <a class="nav-item nav-link" href="{{ route('page.travel.create')}}">Publicar viaje</a>
                   @endif
                    <li class="dropdown user nav-item nav-link">
                    <a class="dropdown-toggle text-success" data-toggle="dropdown" href="#">
                        <i class="icon-user text-success"></i>
                        {{ Auth::user()->fullName }}
                    </a>
                    <ul class="dropdown-menu">

                        <li class="ml-2">
                            <i class="fas fa-cog"></i>
                            <a href="{{ route('page.profile.index',[Auth::user()->id]) }}">
                                <font style="vertical-align: inherit;">
                                    <font class="text-gray1" style="vertical-align: inherit;">Mi perfil</font>
                                </font>
                            </a>
                        </li>
                        <li class="ml-2">
                            <i class="fas fa-list-alt"></i>
                            <a href="{{ route('page.profile.index',[Auth::user()->id]) }}"><font style="vertical-align: inherit;"><font class="text-gray1" style="vertical-align: inherit;">Historial</font></font></a>
                        </li>
                        <li class="divider"></li>
                        <li class="ml-2">
                            <i class="fas fa-arrow-alt-circle-left"></i>
                            <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                                <font style="vertical-align: inherit;">
                                    <font class="text-gray1" style="vertical-align: inherit;">
                                        Cerrar Sesión
                                    </font>
                                </font>
                            </a>
                        </li>
                    </ul>
                    </li>

                @endif
            </div>
        </div>
    </nav>
</div>
