@extends('layouts.frontend.master')
@section('content')
    <!-- Masthead -->
    <header class="masthead text-white text-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-9 mx-auto">
                    <h2 class="text-left">Autos Compartidos</h2>
                    <h4 class="text-left font-weight-light ">Viajes seguro y económicos a todas partes del país</h4>
                    <br>
                </div>
                <div class="col-12 col-md-12 col-lg-9 mx-auto">
                    <form id="form_landing" action="{{ route('page.travel.find.search') }}" method="GET">

{{--                        <div class="input-group">--}}
{{--                            <input type="text" name="search_travel" class="form-control form-control-lg borderiz" placeholder="A donde quieres ir? ">--}}
{{--                            <div class="input-group-prepend">--}}
{{--                                <button type="submit" class="btn btn-block btn-lg btn-success borderder pl-5 pr-5">Buscar</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="d-flex justify-content-center mb-5">--}}
                        <div class="d-flex justify-content-start mb-5">
                            <div class="searchbar">
                                <input autocomplete="off" id="search_input" class="search_input" type="text" name="search_travel" placeholder="Buscar consulta">
                                <a class="search_icon" href="javascript:;" onclick="document.getElementById('form_landing').submit();"><i class="fas fa-search"></i></a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </header>


    <!-- iconos -->
    <section class="features-icons text-left ">
        <div class="container">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-lg-3 col-12 pb-5 pt-5 pl-4 pr-4 m-4 bg-light bordes" >
                    <div>
                        <img src="{{ asset('img/iconos/icono-1.png') }}" style="width: 40%;display : block;margin : auto;" class="mb-4" alt="publica tu viaje">
                        <h5 class="text-success text-center">Publica tu viaje</h5>
                        <p class="font-weight-light text-center">Ahora en tu viaje, compartiendo el costo con otras personas.</p>
                        <br>
                        <br>
                        <br>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('page.travel.create') }}" class="btn btn-success2 borderder borderiz mb-3 pr-5 pl-5 btn-sm ">Publicar Viaje</a>
                    </div>
                </div>

                <div class="col-lg-3 col-12 pb-5 pt-5 pl-4 pr-4 m-4 bg-light bordes ">
                    <div>
                        <img src="{{ asset('img/iconos/icono-2.png') }}" style="width: 40%;display : block;margin : auto;" class="mb-4" alt="viaje económico">
                        <h5 class="text-success text-center">Viaje económico</h5>
                        <p class="font-weight-light text-center">Viaja seguro y a bajo costo compartiendo el auto de otra persona</p>
                        <br>
                        <br>
                    </div>
                    <div class="d-flex justify-content-center">

                        <a href="{{ route('page.travel.find.search') }}" class="btn btn-success2 borderder borderiz mb-3 pr-5 pl-5 btn-sm">Buscar Viaje</a>
                    </div>

                </div>
                <div class="col-lg-3 col-12 pb-5 pt-5 pl-4 pr-4 m-4 bg-light bordes ">
                    <div>
                        <img src="{{ asset('img/iconos/icono-3.png') }}" style="width: 40%;display : block;margin : auto;" class=" mb-4" alt="pago seguro">
                        <h5 class="text-success text-center">Pago seguro</h5>
                        <p class="font-weight-light text-center">Somos intermediarios entre los viajeros, para que el viaje sea seguro y económico</p>
                        <br>
                        <br>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="#" class="btn btn-success2 borderder borderiz mb-3 pr-5 pl-5 btn-sm">&nbsp;&nbsp;&nbsp;Más Información&nbsp;&nbsp;&nbsp;</a>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- viajes frecuentes -->

    <section class="container mt-5 mb-5">
        <h2> Viajes más frecuentes</h2>
        <div class="row">
            <div class=" col-md-10 col-sm-12">
                <p class="font-weight-light "> En los viajes frecuentes encontraras una gran variedad de precios
                </p>
            </div>
            <div class=" col-md-2 col-sm-12">
                <a href="#" class="btn btn-success  borderder borderiz text-decoration-none">
                    <h6 class="font-weight-light pl-3 pr-3 pt-0 pb-0 m-0">Ver todos</h6></a>
            </div>
        </div>
    </section>

    <div class="container pb-5 mb-5">
        <div id="container2">
            <div id="modules">
                <div class="module">
                    <figure class="espacios col-lg-12 col-md-12 d-md-inline-block">
                        <img src="{{ asset('img/CDE.jpg') }}"
                             class="img-fluid img2">
                        <div class="carousel-caption">
                            <h4 class="card-title text-left font-weight-light">Ciudad del Este</h4>
                            <h6 class="card-text font-weight-light text-left mb-4">Saliendo desde Asunción</h6>
                            <h6 class="card-text text-success text-left font-weight-light">Precios desde</h6>
                            <h4 class=" text-left mb-4">Gs. 50.000</h4>
                            <a class="btn btn-sm float-left btn-success2 borderiz borderder pl-5 pr-5" href="#">Reservar</a>
                        </div>
                    </figure>
                </div>
                <div class="module">
                    <figure class="espacios col-lg-12 col-md-12d-md-inline-block  ">
                        <img src="{{ asset('img/encarnacion1.jpg') }}"
                             class="img-fluid img2">
                        <div class="carousel-caption">
                            <h4 class="card-title text-left font-weight-light">Encarnación</h4>
                            <h6 class="card-text font-weight-light text-left mb-4">Saliendo desde Asunción</h6>
                            <h6 class="card-text text-success font-weight-light  text-left">Precios desde</h6>
                            <h4 class=" text-left mb-4">Gs. 100.000</h4>
                            <a class="btn btn-sm float-left btn-success2 borderiz borderder pl-5 pr-5" href="#">Reservar</a>
                        </div>
                    </figure>
                </div>
                <div class="module">
                    <figure class="espacios col-lg-12 col-md-12 d-md-inline-block">
                        <img src="{{ asset('img/PJC.jpg') }}"
                             class="img-fluid img2">
                        <div class="carousel-caption">
                            <h4 class="card-title text-left font-weight-light">Pedro Juan Caballero</h4>
                            <h6 class="card-text font-weight-light text-left mb-4">Saliendo desde Asunción</h6>
                            <h6 class="card-text text-success text-left font-weight-light">Precios desde</h6>
                            <h4 class=" text-left mb-4">Gs. 15.000</h4>
                            <a class="btn btn-sm float-left btn-success2 borderiz borderder pl-5 pr-5" href="#">Reservar</a>
                        </div>
                    </figure>
                </div>
                <div class="module">
                    <figure class="espacios col-lg-12 col-md-12 d-md-inline-block">
                        <img src="{{ asset('img/CDE.jpg') }}"
                             class="img-fluid img2">
                        <div class="carousel-caption">
                            <h4 class="card-title text-left font-weight-light">Ciudad del Este</h4>
                            <h6 class="card-text font-weight-light text-left mb-4">Saliendo desde Asunción</h6>
                            <h6 class="card-text text-success text-left font-weight-light">Precios desde</h6>
                            <h4 class=" text-left mb-4">Gs. 50.000</h4>
                            <a class="btn btn-sm float-left btn-success2 borderiz borderder pl-5 pr-5" href="#">Reservar</a>
                        </div>
                    </figure>
                </div>
                <div class="module">
                    <figure class="espacios col-lg-12 col-md-12 d-md-inline-block">
                        <img src="{{ asset('img/PJC.jpg') }}"
                             class="img-fluid img2">
                        <div class="carousel-caption">
                            <h4 class="card-title text-left font-weight-light">Pedro Juan Caballero</h4>
                            <h6 class="card-text font-weight-light text-left mb-4">Saliendo desde Asunción</h6>
                            <h6 class="card-text text-success text-left font-weight-light">Precios desde</h6>
                            <h4 class=" text-left mb-4">Gs. 15.000</h4>
                            <a class="btn btn-sm float-left btn-success2 borderiz borderder pl-5 pr-5" href="#">Reservar</a>
                        </div>
                    </figure>
                </div>
                <div class="module">
                    <figure class="espacios col-lg-12 col-md-12 d-md-inline-block">
                        <img src="{{ asset('img/PJC.jpg') }}"
                             class="img-fluid img2">
                        <div class="carousel-caption">
                            <h4 class="card-title text-left font-weight-light">Pedro Juan Caballero</h4>
                            <h6 class="card-text font-weight-light text-left mb-4">Saliendo desde Asunción</h6>
                            <h6 class="card-text text-success text-left font-weight-light">Precios desde</h6>
                            <h4 class=" text-left mb-4">Gs. 15.000</h4>
                            <a class="btn btn-sm float-left btn-success2 borderiz borderder pl-5 pr-5" href="#">Reservar</a>
                        </div>
                    </figure>
                </div>
            </div>
        </div>
    </div>

    <!--MAPA-->
{{--    <section class=" ">--}}
{{--        <!-- <div class="text-white "> -->--}}
{{--        <div class="col-md-12 col-lg-12 col-sm-12">--}}
{{--            <div class="row group">--}}
{{--                <div class="bg-light col-lg-5 col-md-5 col-sm-12 p-5">--}}
{{--                    <h4 class="text-success ">Busca tu Viaje</h4>--}}
{{--                    <h6 class="mb-5  font-weight-light"> Lorem Ipsum is simply dummy text of the printing and typesetting industry.  </h6>--}}

{{--                    <form>--}}
{{--                        <div class="form-row">--}}
{{--                            <div class="col-sm-12 mb-4">--}}
{{--                                <input type="text" class="form-control borderder borderiz" placeholder="Ciudad de Salida">--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-12 mb-4 ">--}}
{{--                                <input type="text" class="form-control borderder borderiz" placeholder="Ciudad de Destino">--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-12 mb-5 mt-4 ">--}}
{{--                                <input type="text" class="form-control borderder borderiz" placeholder="Fecha de Viaje">--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-12  ">--}}
{{--                                <button type="submit" class="btn btn-block btn-dark borderder borderiz">Buscar Viaje</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}


{{--                </div>--}}
{{--                <div class="col-md-7 col-lg-7  col-sm-12 espacios" >--}}
{{--                    <!--Google map-->--}}
{{--                    <div id="map-container-google-11" class="z-depth-1-half map-container-6">--}}
{{--                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4290.935788473743!2d-57.58662018530905!3d-25.262762359850566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da7e17491fb35%3A0xb14b9f67e99a634c!2sLa%20F%C3%A1brica%20CoWorking!5e0!3m2!1ses-419!2spy!4v1569110225220!5m2!1ses-419!2spy" frameborder="0" style=" border:0;" allowfullscreen=""></iframe>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!-- </div> -->--}}
{{--    </section>--}}

    <!--Preguntas Frecuentas-->


    <section class="features-icons pl-5 pr-5 ">
        <section class="container col-md-12 col-lg-12 col-xl-9 mt-5 mb-5">
            <h2 class="mb-5 text-center"> Preguntas frecuentes</h2>

{{--            <div class="d-flex justify-content-center mb-5">--}}
{{--                <div class="searchbar">--}}
{{--                    <input class="search_input" type="text" name="" placeholder="Buscar consulta">--}}
{{--                    <a href="#" class="search_icon"><i class="fas fa-search"></i></a>--}}
{{--                </div>--}}
{{--            </div>--}}

            <div id="accordionExample">
                <div class="ml-5 mr-5">
                    <div class=" mx-auto mb-3">
                        <button class="btn btn-block btn-success3 btn-lg borderder borderiz font-weight-light text-left pl-4 pr-4" data-toggle="collapse" data-target="#collapseOne" >
                            ¿Qué es viajes Compartid2?
                        </button>
                    </div>

                    <div id="collapseOne" class="collapse font-weight-light " data-parent="#accordionExample">
                        <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="ml-5 mr-5" >
                    <div class=" mx-auto mb-3" id="headingTwo">
                        <button class="btn  collapsed btn-block btn-success3 btn-lg borderder borderiz font-weight-light text-left pl-4 pr-4" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            ¿Cómo funciona Compartid2?
                        </button>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="ml-5 mr-5">
                    <div class=" mx-auto mb-3" id="headingThree">
                        <button class="btn btn-block btn-success3 btn-lg borderder borderiz font-weight-light text-left pl-4 pr-4 collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            ¿Cómo funciona Compartid2?
                        </button>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="ml-5 mr-5">
                    <div id="collapse4" class=" mx-auto mb-3">
                        <button class="btn btn-block btn-success3 btn-lg borderder borderiz font-weight-light text-left pl-4 pr-4" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                            ¿Cómo funciona Compartid2?
                        </button>
                    </div>

                    <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionExample">
                        <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="ml-5 mr-5" >
                    <div class=" mx-auto mb-3" id="heading5">
                        <button class="btn  collapsed btn-block btn-success3 btn-lg borderder borderiz font-weight-light text-left pl-4 pr-4" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            ¿Cómo funciona Compartid2?
                        </button>
                    </div>
                    <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordionExample">
                        <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="ml-5 mr-5">
                    <div class=" mx-auto mb-3" id="heading6">
                        <button class="btn btn-block btn-success3 btn-lg borderder borderiz font-weight-light text-left pl-4 pr-4 collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                            ¿Cómo funciona Compartid2?
                        </button>
                    </div>
                    <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionExample">
                        <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </section>


    <!-- Suscripción -->
    <section class="bg-success text-white text-lg-left pt-5 pb-5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-9 mx-auto">
                    <h3 class="mb-2">Suscribete a nuestro Newsletter</h3>
                    <p class="font-weight-light"> No te pierdas de todas nuestras novedades</p>
                </div>
                <div class="col-md-12 col-lg-12 col-xl-9 mx-auto">

                    <form>
                        <div class="form-row">

                            <div class="input-group">
                                <input type="email" class="form-control form-control-lg pl-4 borderiz" placeholder=" Ingresa tu correo electrónico ">
                                <div class="input-group-prepend">

                                    <button type="submit" class="btn btn-dark btn-block btn-lg borderder">Suscribirme</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNxZ-cRjMfolNAXUdSqtZd_BgtClBtQ7M&libraries=places,geometry&callback=initialize" async defer></script>
    <script src="{{ asset('js/maps_customs.js') }}"></script>
    <script>
        function initialize() {
            $('form').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });

            search();
        }

        function search() {
            const endInputs = document.getElementsByClassName("search_input");
            const autocompletes_search = [];
            const geocoder = new google.maps.Geocoder;

            for (let i = 0; i < endInputs.length; i++) {

                const input = endInputs[i];
                const fieldKey = input.id.replace("-input", "");

                const autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.key = fieldKey;
                autocompletes_search.push({input: input, autocomplete: autocomplete});
            }
        }
    </script>
@endsection
