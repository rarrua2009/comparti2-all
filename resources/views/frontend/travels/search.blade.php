@extends('layouts.frontend.master')
@section('style')
    <style type="text/css">
        .borderder{
            border-top-right-radius: 20px !important;
            border-bottom-right-radius: 20px !important;
        }


        .borderiz{
            border-top-left-radius: 20px !important;
            border-bottom-left-radius: 20px !important;
        }

        .borderder1{
            border-top-right-radius: 10px !important;
            border-bottom-right-radius: 10px !important;
        }


        .borderiz1{
            border-top-left-radius: 10px !important;
            border-bottom-left-radius: 10px !important;
        }
        .box{
            width: 100%;
            height: 100%;
        }


        .espacios{
            padding: 0px !important;
            width: auto;
            height: 100%;
        }
        .btn-circle {
            width: 50px;
            height: 50px;

            font-size: 18px;
            line-height: 1.33;
            border-radius: 25px;

        }

        .grid1{

            display: grid;
            grid-gap: 10px;
            grid-template-columns: 40px 1fr 1fr 1fr 100px 40px;
            grid-template-rows: auto auto auto auto auto;
            grid-template-areas: "vacio1 encabezado1 encabezado1 encabezado2 encabezado3 vacio2"
            "vacio1 item1 item1 item4 item4 vacio2"
            "vacio1 item2 item2 item4 item4 vacio2"
            "vacio1 item3 item3 item4 item4 vacio2";


        }

        .item1 {

            grid-area: item1;
        }

        .item2 {
            grid-area: item2;
        }

        .item3 {
            grid-area: item3;
        }

        .item4 {
            grid-area: item4;
        }

        .encabezado1{
            grid-area: encabezado1;
        }

        .encabezado2{
            grid-area: encabezado2;
        }

        .encabezado3{
            grid-area: encabezado3;
        }

        .vacio1 {
            grid-area: vacio1;
        }
        .vacio2 {
            grid-area: vacio2;
        }

        .circulo{
            width: 50px;
            height: 50px;
            padding-right: 1px;
            border-top-right-radius: 50px !important;
            border-bottom-right-radius: 50px !important;
            border-top-left-radius: 50px !important;
            border-bottom-left-radius: 50px !important;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        @media screen and (max-width: 768px) {
            .grid1{
                grid-template-areas:"encabezado1 encabezado1 encabezado1 encabezado1 encabezado2 encabezado3"
                "item1        item1       item1       item1       item1       item1"
                "item2        item2       item2       item2       item2       item2"
                "item3        item3       item3       item3       item3       item3"
                "item4        item4       item4       item4       item4       item4";


            }


            h1 {
                font-size: 1.25rem;
                padding-top: 5px !important;
            }

            h5 {
                font-size: 1rem;
                margin-top: 0px !important;
            }

        }

    </style>
    <link href="{{ asset('vendor/pickerjs/themes/default.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pickerjs/themes/default.date.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pickerjs/themes/default.time.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section class= "bg-success pt-5 pb-5">
        <div class="container">

            <h3 class="text-white">Busca tu Viaje</h3>
            <br>
            <form id="form_search_request" action="{{ route('page.travel.find.search') }}" method="GET">
                <div class="form-row">
                    <div class="col-sm-12 col-md-3 col-lg-3 form-group">
                        <input type="text" name="search_travel_origin" class="search_travel_origin form-control borderder borderiz" placeholder="Ciudad de Salida">
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 form-group">
                        <input type="text" name="search_travel_destiny" class="search_travel_destiny form-control borderder borderiz" placeholder="Ciudad de Destino">
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 form-group">
                        <input type="text" name="date_of_travel" id="date_of_travel" class="form-control borderder borderiz" placeholder="Fecha de Viaje">
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 form-group">
                        <button type="submit" class="btn btn-block btn-dark borderder borderiz">Buscar Viaje</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

    @if(isset($search))
        <div class="container pt-2 pb-5 mb-5">
            <div id="container2">
                @foreach($search as $path)
                    <div class="module">
                        <figure class="espacios col-lg-12 col-md-12d-md-inline-block  ">
                            <img src="{{ asset('img/encarnacion1.jpg') }}"
                                 class="img-fluid img2">
                            <div class="carousel-caption">
                                <h4 class="card-title text-left font-weight-light">{{ $path->destination }}</h4>
                                <h6 class="card-text font-weight-light text-left mb-4">{{ $path->origin }}</h6>
                                <h6 class="card-text text-success font-weight-light  text-left">Precios desde</h6>
                                <h4 class=" text-left mb-4">Gs. {{ $path->travel->price }}</h4>
                                @if(Auth::check() && Auth::user()->type == 'passenger')
                                    <a class="btn btn-primary btn-sm float-left btn-success2 borderiz borderder m-1 pl-5 pr-5" href="{{ route('page.travel.reserve',$path->id) }}">Reservar</a>
                                    <a class="btn btn-primary btn-sm float-left btn-success2 borderiz borderder m-1 pl-5 pr-5" href="{{ route('page.travel.details',$path->travel->id) }}">Detalles</a>
                                @else
                                    <a class="btn btn-primary btn-sm float-left btn-success2 borderiz borderder pl-5 pr-5" href="#">Reservar</a>
                                @endif
                            </div>
                        </figure>
                    </div>
                @endforeach
            </div>
            {{ $search->links() }}
        </div>
    @endif




@endsection
@section('scripts')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNxZ-cRjMfolNAXUdSqtZd_BgtClBtQ7M&libraries=places,geometry&callback=initialize" async defer></script>
    <script src="{{ asset('vendor/pickerjs/legacy.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.date.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.time.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/translations/es_ES.js') }}"></script>

    <script>
        $(function () {

            $('#date_of_travel').pickadate({
                min:new Date('today'),
                format: 'd mmmm, yyyy',
                formatSubmit: 'yyyy-mm-dd',

            });
        });
    </script>
    <script>
        function initialize() {
            $('form').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });

            search_origin();
            search_end();
        }

        function search_origin() {
            const endInputs = document.getElementsByClassName("search_travel_origin");
            const autocompletes_origin = [];
            const geocoder = new google.maps.Geocoder;

            for (let i = 0; i < endInputs.length; i++) {

                const input = endInputs[i];
                const fieldKey = input.id.replace("-input", "");

                const autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.key = fieldKey;
                autocompletes_origin.push({input: input, autocomplete: autocomplete});
            }
        }

        function search_end() {
            const endInputs = document.getElementsByClassName("search_travel_destiny");
            const autocompletes_destiny = [];
            const geocoder = new google.maps.Geocoder;

            for (let i = 0; i < endInputs.length; i++) {

                const input = endInputs[i];
                const fieldKey = input.id.replace("-input", "");

                const autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.key = fieldKey;
                autocompletes_destiny.push({input: input, autocomplete: autocomplete});
            }
        }
    </script>
@endsection
