@extends('layouts.frontend.master')
@section('style')
    <link href="{{ asset('vendor/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/select2/css/select2-bootstrap4.min.css') }}" rel="stylesheet">
    <style type="text/css">
        .borderder{
            border-top-right-radius: 20px !important;
            border-bottom-right-radius: 20px !important;
        }


        .borderiz{
            border-top-left-radius: 20px !important;
            border-bottom-left-radius: 20px !important;
        }

        .borderder1{
            border-top-right-radius: 10px !important;
            border-bottom-right-radius: 10px !important;
        }


        .borderiz1{
            border-top-left-radius: 10px !important;
            border-bottom-left-radius: 10px !important;
        }
        .box{
            width: 100%;
            height: 100%;
        }


        .espacios{
            padding: 0px !important;
            width: auto;
            height: 100%;
        }
        .btn-circle {
            width: 50px;
            height: 50px;

            font-size: 18px;
            line-height: 1.33;
            border-radius: 25px;

        }

        .grid1{

            display: grid;
            grid-gap: 10px;
            grid-template-columns: 40px 1fr 1fr 1fr 100px 40px;
            grid-template-rows: auto auto auto auto auto;
            grid-template-areas: "vacio1 encabezado1 encabezado1 encabezado2 encabezado3 vacio2"
            "vacio1 item1 item1 item4 item4 vacio2"
            "vacio1 item2 item2 item4 item4 vacio2"
            "vacio1 item3 item3 item4 item4 vacio2";


        }

        .item1 {

            grid-area: item1;
        }

        .item2 {
            grid-area: item2;
        }

        .item3 {
            grid-area: item3;
        }

        .item4 {
            grid-area: item4;
        }

        .encabezado1{
            grid-area: encabezado1;
        }

        .encabezado2{
            grid-area: encabezado2;
        }

        .encabezado3{
            grid-area: encabezado3;
        }

        .vacio1 {
            grid-area: vacio1;
        }
        .vacio2 {
            grid-area: vacio2;
        }

        .circulo{
            width: 50px;
            height: 50px;
            padding-right: 1px;
            border-top-right-radius: 50px !important;
            border-bottom-right-radius: 50px !important;
            border-top-left-radius: 50px !important;
            border-bottom-left-radius: 50px !important;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        @media screen and (max-width: 768px) {
            .grid1{
                grid-template-areas:"encabezado1 encabezado1 encabezado1 encabezado1 encabezado2 encabezado3"
                "item1        item1       item1       item1       item1       item1"
                "item2        item2       item2       item2       item2       item2"
                "item3        item3       item3       item3       item3       item3"
                "item4        item4       item4       item4       item4       item4";


            }


            h1 {
                font-size: 1.25rem;
                padding-top: 5px !important;
            }

            h5 {
                font-size: 1rem;
                margin-top: 0px !important;
            }

        }

    </style>
    <link href="{{ asset('vendor/pickerjs/themes/default.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pickerjs/themes/default.date.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pickerjs/themes/default.time.css') }}" rel="stylesheet">
@endsection
@section('content')
{{--    <form action="{{ route('page.travel.store') }}" method="POST">--}}
{{--        @csrf--}}
        <div class="bg-light pt-5">
            <div class="container">
                <div class="grid1">

                    <div class="vacio1"></div>
                    <div class="vacio2"></div>
                    <h1 class="encabezado1 mt-3">Detalles del viaje</h1>
                    <h5 class="encabezado2 text-lg-right text-success mt-4">Detalles</h5>
{{--                    <div class="encabezado3 text-white circulo bg-success mt-2">--}}
{{--                        <h5 class=" pt-1 text-center ">1/4</h5>--}}
{{--                    </div>--}}


                    <div class="item1 pl-3 pr-3 pt-3 pb-3 bg-gray1 borderiz1 borderder1" id="contenedor1">
                        <div class="form-row">
                            <div class="form-group col-lg-12">
                                <label for="address-input" class="text-white ml-4">Lugar de salida</label>
                                <input type="text" autocomplete="false" id="address-input" class="form-control map-input" value="{{ $travel->begin_address }}" readonly>
                            </div>
                            <div class="form-group col-lg-12">
                                <label class="text-white ml-4" for="llegada-input">Lugar de llegada</label>
                                <input type="text" id="llegada-input" class="mt-2 mb-2 form-control borderiz borderder end-input"  value="{{ $travel->end_address }}">
                            </div>
                            <div class="form-group col-lg-12">
                                <label class="text-white ml-4" for="price">Precio por persona</label>
                                <input type="text" name="price" class="ph font-weight-bold h7 form-control borderiz borderder bg-blanco mt-3 mb-4 text-success text-center pt-2 pb-2 " id="price" value="GS. {{ $travel->price }}" readonly>
                            </div>
                        </div>

                    </div>

                    @if($travel->stop_address != null)
                    <div class="item2 pl-3 pr-3 pt-3 pb-3 bg-gray1 borderder1 borderiz1" id="contenedor2">
                        <div class="form-group col-lg-12">
                            <label class="text-white ml-4" for="stop_address">Parada</label>
                            <input type="text" name="stop_address" class="form-control borderiz borderder stop-input" id="stop_address"  value="{{ $travel->stop_address }}">
                        </div>
                        <div class="form-group col-lg-12">
                            <label class="text-white ml-4" for="price_to_stop">Precio hasta la parada</label>
                            <input type="text" name="price_to_stop" class="ph font-weight-bold h7 form-control borderiz borderder bg-blanco mt-3 mb-4 text-success text-center pt-2 pb-2" id="price_to_stop" value="GS. {{ $travel->price_to_stop }}" readonly>
                        </div>
                    </div>
                    @endif

                    <div class="item3 pl-3 pr-3 pt-3 pb-3 bg-gray1 borderder1 borderiz1" id="contenedor3">

                        <div class="form-row">
                            <div class="form-group col-lg-6  ">
                                <label class="text-white ml-4" for="fecha_salida">Fecha de Salida</label>
                                <input  type="text" class="form-control borderder borderiz text-center" data-value={{ $travel->departure_date_time->format('d/m/Y') }} name="fecha_salida" id="fecha_salida" readonly >
                            </div>

                            <div class="form-group col-lg-6  ">
                                <label class="text-white ml-4" for="hora_salida">Hora de Salida</label>
                                <input  type="text" class="form-control borderder borderiz text-center" data-value={{ $travel->departure_date_time->format('H:i') }} name="hora_salida" id="hora_salida" readonly>
                                @error('hora_salida')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-lg-6  ">
                                <label for="seat" class="text-white ml-4">Asientos disponibles</label>
                                <input type="text" class="form-control borderder borderiz text-center" name="seat" id="seat" value="{{ $travel->seat }}" readonly>
                            </div>
                            <div class="form-group col-lg-6  ">
                                <label class="text-white ml-4" for="car">Vehiculo</label>
                                <input type="text" class="form-control borderder borderiz text-center" name="seat" id="seat" value="{{ $travel->car->brand .' - ' .$travel->car->model  }}" readonly>

                            </div>

                        </div>

                        <div class="form-row form-check">
                            <input type="checkbox" id="round_trip" name="round_trip" class="pl-3 pr-5 form-check-input" {{ ($travel->paths->round_trip) ? 'checked':'' }}>
                            <label class="mr-4 ml-4 form-check-label" for="round_trip">Viaje de vuelta</label>
                        </div>
                    </div>

                    <section class="item4"  id="address-map-container" style="width:100%;height:400px; ">
                        <div style="width: 100%; height: 100%" id="address-map"></div>
                    </section>

                </div>
            </div>

        </div>
        <div class="bg-light pt-5 pb-5">
            <div class="container mb-5">
                <div class="container">
                    <div class="container">
                        <div class="container">
                            <button type="button" class="mb-5 col-lg-3 col-md-12 btn btn-dark borderiz borderder" onclick="window.history.go(-1); return false;">Atras </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
{{--    </form>--}}
@endsection
@section('scripts')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNxZ-cRjMfolNAXUdSqtZd_BgtClBtQ7M&libraries=places,geometry&callback=initialize" async defer></script>
{{--    <script src="{{ asset('js/maps_customs.js') }}"></script>--}}
    <script src="{{ asset('vendor/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/legacy.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.date.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.time.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/translations/es_ES.js') }}"></script>
    <script>
        $(function () {
            $('#fecha_salida').pickadate({
                min:new Date('today'),
                format: 'd mmmm, yyyy',
                formatSubmit: 'yyyy-mm-dd',

            });
            $('#hora_salida').pickatime({
                format: 'HH:i',
                formatSubmit: 'HH:i',
            })
        });

        function initialize() {
            $('form').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });

            const map = new google.maps.Map(document.getElementById('address-map'), {
                center: {lat: -33.8688, lng: 151.2195},
                zoom: 13,
                gestureHandling: 'cooperative'
            });



            var marker2 = new google.maps.Marker({
                map: map,
                position: {lat: {!!  $travel->begin_location_lat !!}, lng: {!!  $travel->begin_location_long !!} },
            });

            var marker3 = new google.maps.Marker({
                map: map,
                position: {lat: {!!  $travel->end_location_lat  !!}, lng: {!!  $travel->end_location_long !!} },
            });

            @if($travel->paths->round_trip)
                var marker = new google.maps.Marker({
                    map: map,
                    position: {lat: {!! $travel->stop_lat !!}, lng: {!! $travel->stop_long !!} },
                });
            @endif

        }
    </script>
@endsection
