@extends('layouts.frontend.master')
@section('style')
    <style type="text/css">
        .borderder{
            border-top-right-radius: 20px !important;
            border-bottom-right-radius: 20px !important;
        }


        .borderiz{
            border-top-left-radius: 20px !important;
            border-bottom-left-radius: 20px !important;
        }

        .borderder1{
            border-top-right-radius: 10px !important;
            border-bottom-right-radius: 10px !important;
        }


        .borderiz1{
            border-top-left-radius: 10px !important;
            border-bottom-left-radius: 10px !important;
        }
        .box{
            width: 100%;
            height: 100%;
        }


        .espacios{
            padding: 0px !important;
            width: auto;
            height: 100%;
        }
        .btn-circle {
            width: 50px;
            height: 50px;

            font-size: 18px;
            line-height: 1.33;
            border-radius: 25px;

        }

        .grid1{

            display: grid;
            grid-gap: 10px;
            grid-template-columns: 40px 1fr 1fr 1fr 100px 40px;
            grid-template-rows: auto auto auto auto auto;
            grid-template-areas: "vacio1 encabezado1 encabezado1 encabezado2 encabezado3 vacio2"
            "vacio1 item1 item1 item4 item4 vacio2"
            "vacio1 item2 item2 item4 item4 vacio2"
            "vacio1 item3 item3 item4 item4 vacio2";


        }

        .item1 {

            grid-area: item1;
        }

        .item2 {
            grid-area: item2;
        }

        .item3 {
            grid-area: item3;
        }

        .item4 {
            grid-area: item4;
        }

        .encabezado1{
            grid-area: encabezado1;
        }

        .encabezado2{
            grid-area: encabezado2;
        }

        .encabezado3{
            grid-area: encabezado3;
        }

        .vacio1 {
            grid-area: vacio1;
        }
        .vacio2 {
            grid-area: vacio2;
        }

        .circulo{
            width: 50px;
            height: 50px;
            padding-right: 1px;
            border-top-right-radius: 50px !important;
            border-bottom-right-radius: 50px !important;
            border-top-left-radius: 50px !important;
            border-bottom-left-radius: 50px !important;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        @media screen and (max-width: 768px) {
            .grid1{
                grid-template-areas:"encabezado1 encabezado1 encabezado1 encabezado1 encabezado2 encabezado3"
                "item1        item1       item1       item1       item1       item1"
                "item2        item2       item2       item2       item2       item2"
                "item3        item3       item3       item3       item3       item3"
                "item4        item4       item4       item4       item4       item4";


            }


            h1 {
                font-size: 1.25rem;
                padding-top: 5px !important;
            }

            h5 {
                font-size: 1rem;
                margin-top: 0px !important;
            }

        }

    </style>
@endsection
@section('content')
    <section class= "bg-success pt-5 pb-5">
        <div class="container">

            <h3 class="text-white">Busca tu Viaje</h3>
            <br>
            <form>
                <div class="form-row">
                    <div class="col-sm-12 col-md-3 col-lg-3 form-group">
                        <input type="text" class="form-control borderder borderiz" placeholder="Ciudad de Salida">
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 form-group">
                        <input type="text" class="form-control borderder borderiz" placeholder="Ciudad de Destino">
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 form-group">
                        <input type="text" class="form-control borderder borderiz" placeholder="Fecha de Viaje">
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 form-group">
                        <button type="submit" class="btn btn-block btn-dark borderder borderiz">Buscar Viaje</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <!--MAPA-->
    <section class="showcase ">
        <!-- <div class="text-white "> -->
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row group">
                <figure class="col-md-3 col-lg-3 col-sm-12 espacios" style="margin-bottom: 0px !important;">
                    <img src="{{ asset('img/encarnacion1.jpg') }}" class="box">
                    <div class="carousel-caption">
                        <h5 class="card-title">Encarnación</h5>
                        <h6 class="card-text font-weight-light">Saliendo desde Asunción</h6>
                        <h6 class="card-text">Precios desde</h6>
                        <h5 class="text-success">Gs. 100.000</h5>
                        <a class="btn btn-sm btn-block btn-gray1 borderiz borderder" href="#">Buscar viaje</a>
                    </div>
                </figure>
                <div class="col-md-9 col-lg-9  col-sm-12 espacios" >
                    <!--Google map-->
                    <div id="map-container-google-11" class="z-depth-1-half map-container-6">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4290.935788473743!2d-57.58662018530905!3d-25.262762359850566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da7e17491fb35%3A0xb14b9f67e99a634c!2sLa%20F%C3%A1brica%20CoWorking!5e0!3m2!1ses-419!2spy!4v1569110225220!5m2!1ses-419!2spy" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- </div> -->
    </section>
@endsection
