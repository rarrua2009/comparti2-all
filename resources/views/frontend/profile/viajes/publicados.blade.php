@extends('layouts.frontend.profile.master')
@section('page-content')
    <div class="container grupo1 pl-5">
        <div class="row mb-5" >
            <h4 class="mt-4 font-weight-light">Viajes publicados</h4>
        </div>

        <div class="row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Salida</th>
                    <th>Destino</th>
                    <th>Dia/Hora</th>
                    <th>Auto</th>
                    <th>Asientos</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($travels as $travel)
                    <tr>
                        <td>{{ $travel->begin_address }}</td>
                        <td>{{ $travel->end_address }}</td>
                        <td>{{ $travel->departure_date_time->format('d/m/Y H:i') }}</td>
                        <td>{{ $travel->car->name }} - {{ $travel->car->model }} - {{ $travel->car->brand }}</td>
                        <td>{{ $travel->seat }}</td>
                        <td>
                            <a href="{{ route('page.profile.travels.created.edit',$travel->id) }}" class="btn btn-sm"><i class="fa fa-edit"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
