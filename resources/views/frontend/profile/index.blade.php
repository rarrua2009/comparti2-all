@extends('layouts.frontend.profile.master')
@section('style')
    <link href="{{ asset('vendor/pickerjs/themes/default.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pickerjs/themes/default.date.css') }}" rel="stylesheet">
@endsection
@section('page-content')
    <div class="container grupo1 pl-5">
        <div class="row mb-5" >
            <h4 class="mt-4 font-weight-light">Resumen de la cuenta</h4>
        </div>

        <div class="row bg-light borderiz borderder mb-5 pl-4 pt-4 pb-4 pr-0">

            <div class="col-lg-1 col-sm-12">
                <h6 class="text-success">Edad</h6>
                <h5 class="font-weight-bold">{{ $customer->date_birth->age }}</h5>
            </div>
            <div class="col-lg-2 col-sm-12">
                <h6 class="text-success">Tipo</h6>
                <toggle-button @change="updateCustomerType($event, '{!! Auth::id() !!}')"
                               @if($customer->type == 'passenger')
                                :value="toggle_data = true"
                               @else
                                :value="toggle_data = false"
                               @endif
                               :sync="true"
                               :labels="{checked: 'Pasajero', unchecked: 'Viajero'}"
                               :width="78" :height="32">
                </toggle-button>
            </div>
            <div class="col-lg-3 col-sm-12">
                <h6 class="text-success">Sexo</h6>
                <h5>{{ $customer->sex }}</h5>
            </div>
            <div class="col-lg-3 col-sm-12">
                <h6 class="text-success">Celular</h6>
                <h5>{{ $customer->phone }}</h5>
            </div>
            <div class="col-lg-3 col-sm-12">
                <h6 class="text-success">Registrado</h6>
                <h5>{{ $customer->created_at->format('d/m/Y') }}</h5>
            </div>
        </div>
        <div class="row mb-4">
            <h4 class="mt-4 font-weight-light">Editar Información</h4>
        </div>


        <form action="{{ route('page.profile.update',$customer->id) }}" method="POST">
            @csrf
            @method('PUT')
        <div class="row">
            <div class="col-lg-6">
                <input type="text" class="bg-light mb-3 form-control borderiz borderder" name="firstname" value="{{ $customer->firstname }}">
                @error('firstname')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <input type="text" class="bg-light mb-3 form-control borderiz borderder" name="lastname" value="{{ $customer->lastname }}">
                @error('lastname')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <input type="email" class="bg-light mb-3 form-control borderiz borderder" name="email" value="{{ $customer->email}}">
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <input type="text"  id="date_birth" name="date_birth" class="bg-light mb-3 form-control borderiz borderder" data-value="{{ $customer->date_birth->format('Y-m-d') }}">
                @error('date_birth')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

{{--        <div class="row">--}}
{{--            <div class="col-lg-6">--}}
{{--                <select class="bg-light form-control borderiz borderder mb-3 " name="type">--}}
{{--                    <option value="passenger" {{ ($customer->type =='passenger') ? 'selected' : '' }}>Pasajero</option>--}}
{{--                    <option value="traveler" {{ ($customer->type =='traveler') ? 'selected' : '' }}>Viajero</option>--}}
{{--                </select>--}}
{{--                @error('type')--}}
{{--                    <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                @enderror--}}
{{--            </div>--}}

{{--        </div>--}}


        <div class="row">
            <div class="col-lg-6">
                <select class="bg-light form-control borderiz borderder mb-3 " name="sex">
                    <option value="masculino" {{ ($customer->sex =='masculino') ? 'selected' : '' }}>Masculino</option>
                    <option value="femenino" {{ ($customer->sex =='femenino') ? 'selected' : '' }}>Femenino</option>
                </select>

            </div>

        </div>

        <div class="row">
            <div class="col-lg-6">
                <input type="number" name="phone" class="bg-light mb-3 form-control borderiz borderder" id="exampleInputPassword1" value="{{ $customer->phone }}">

            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <button class="btn btn-dark btn-block borderiz borderder mb-3" type="submit" >Realizar Cambios</button>
            </div>
        </div>
        </form>
        <div class="row mb-4">
            <h4 class="mt-4 font-weight-light">Cambiar contraseña</h4>
        </div>
        <form action="{{ route('page.profile.update.password') }}" method="POST">
            @csrf
        <div class="row mb-1">
            <h4 class="mt-1 font-weight-light">Contraseña nueva</h4>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <input type="password" class="bg-light mb-5  form-control borderiz borderder" name="password">
                @error('password')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row mb-1">
            <h4 class="mt-1 font-weight-light">Confirmar nueva Contraseña</h4>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <input type="password" class="bg-light mb-5  form-control borderiz borderder" name="password_confirmation">
                @error('password_confirmation')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <button class="btn btn-dark btn-block borderiz borderder mb-3" type="submit" >Cambiar contraseña</button>
            </div>
        </div>


        </form>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/frontend.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/legacy.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.date.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/translations/es_ES.js') }}"></script>

    <script type="text/javascript">
        // Your code goes here.
        $(document).ready(function () {
            $('#date_birth').pickadate({
                selectYears: 80,
                min:new Date(1940,0,1),
                max: new Date(2002,0,1),
                selectMonths: true,
                format: 'd mmmm, yyyy',
                formatSubmit: 'yyyy-mm-dd',
                hiddenPrefix: undefined,
                hiddenName: 'date_birth_submited',
            });
        })
    </script>
@endsection
