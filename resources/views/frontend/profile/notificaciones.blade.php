@extends('layouts.frontend.profile.master')
@section('page-content')
    <div class="container grupo1 pl-5">
        <div class="row mb-5" >
            <h4 class="mt-4 font-weight-light">Notificaciones</h4>
        </div>
        <div class="row">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Descripcion</th>
                        <th>Tipo</th>
                        <th>Usuario</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($notifications as $notification)
                        <tr>
                            <td>{{ $notification->content }}</td>
                            <td>{{ $notification->type }}</td>
                            <td>{{ $notification->user->fullname }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
