@extends('layouts.frontend.profile.master')
@section('style')
@endsection
@section('page-content')
    <div class="container grupo1 pl-5">
        <div class="row mb-5" >
            <h4 class="mt-4 font-weight-light">Agregar un nuevo auto</h4>
        </div>


    <form action="{{ route('page.profile.cars.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-lg-6">
                <label for="name">Nombre</label>
                <input type="text" id="name" class="bg-light mb-3 form-control borderiz borderder" name="name" value="{{ old('name') }}">
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label for="brand">Marca</label>
                <select class="bg-light mb-3 form-control borderiz borderder" name="brand" id="brand">
                    @foreach($marcas as $marca)
                        <option value="{{ $marca }}">{{ $marca }}</option>
                    @endforeach
                </select>
{{--                <input type="text" id="brand" class="bg-light mb-3 form-control borderiz borderder" name="brand" value="{{ old('brand') }}">--}}
                @error('brand')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <label for="model">Modelo</label>
                <input type="text" id="model" class="bg-light mb-3 form-control borderiz borderder" name="model" value="{{ old('model') }}">
                @error('model')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <label for="year">Año</label>
                <select id="year" name="year" class="bg-light mb-3 form-control borderiz borderder">
                    @for ($i = 1995; $i <= \Carbon\Carbon::now()->year; $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
                @error('year')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <label for="plate">Foto de chapa</label>
                <input type="text" id="plate" class="bg-light mb-3 form-control borderiz borderder" name="plate">
                @error('plate')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <label for="license_front">Licencia de conducir (frente)</label>
                <input type="file" id="license_front" class="bg-light mb-3 form-control borderiz borderder" name="license_front" alt="Foto de licencia de conducir">
                @error('license_front')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-lg-3">
                <label for="license_back">Licencia de conducir (reverso)</label>
                <input type="file" id="license_back" class="bg-light mb-3 form-control borderiz borderder" name="license_back" alt="Foto de licencia de conducir">
                @error('license_back')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3">
                <label for="green_license_front">Cedula verde (frente)</label>
                <input type="file" id="green_license_front" class="bg-light mb-3 form-control borderiz borderder" name="green_license_front" alt="Foto de la cedula verd">
                @error('green_license_front')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-lg-3">
                <label for="green_license_back">Cedula verde (reverso)</label>
                <input type="file" id="green_license_back" class="bg-light mb-3 form-control borderiz borderder" name="green_license_back" alt="Foto de la cedula verde">
                @error('green_license_back')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <button class="btn btn-dark btn-block borderiz borderder mb-3" type="submit" >Agregar</button>
            </div>
        </div>

    </form>
    </div>
@endsection
@section('scripts')

    <script type="text/javascript">
        // Your code goes here.
        $(document).ready(function () {
        })
    </script>
@endsection
