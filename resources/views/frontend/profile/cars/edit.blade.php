@extends('layouts.frontend.profile.master')
@section('style')
    <link href="{{ asset('vendor/pickerjs/themes/default.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pickerjs/themes/default.date.css') }}" rel="stylesheet">
@endsection
@section('page-content')
    <div class="container grupo1 pl-5">
        <div class="row mb-5" >
            <h4 class="mt-4 font-weight-light">Editar auto</h4>
        </div>
    </div>

    <form action="{{ route('page.profile.cars.update',$car->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-lg-6">
                <label for="name">Nombre</label>
                <input type="text" id="name" class="bg-light mb-3 form-control borderiz borderder" name="name" value="{{ old('name',$car->name) }}">
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <label for="brand">Marca</label>
                <input type="text" id="brand" class="bg-light mb-3 form-control borderiz borderder" name="brand" value="{{ old('brand',$car->brand) }}">
                @error('brand')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <label for="model">Modelo</label>
                <input type="text" id="model" class="bg-light mb-3 form-control borderiz borderder" name="model" value="{{ old('model',$car->model) }}">
                @error('model')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

{{--        <div class="row">--}}
{{--            <div class="col-lg-6">--}}
{{--                <label for="year">Año</label>--}}
{{--                <input type="text"  id="year" name="year" class="bg-light mb-3 form-control borderiz borderder" data-value="{{ $car->year }}">--}}
{{--                @error('year')--}}
{{--                    <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                @enderror--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="row">
            <div class="col-lg-6">
                <label for="year">Año</label>
                <select id="year" name="year" class="bg-light mb-3 form-control borderiz borderder">
                    @for ($i = 1995; $i <= \Carbon\Carbon::now()->year; $i++)
                        <option value="{{ $i }}" {{ ($car->year == $i) ? 'selected': '' }}>{{ $i }}</option>
                    @endfor
                </select>
                @error('year')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <button class="btn btn-dark btn-block borderiz borderder mb-3" type="submit" >Actualizar</button>
            </div>
        </div>

    </form>
@endsection
@section('scripts')
    <script src="{{ asset('vendor/pickerjs/legacy.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/picker.date.js') }}"></script>
    <script src="{{ asset('vendor/pickerjs/translations/es_ES.js') }}"></script>

    <script type="text/javascript">
        // Your code goes here.
        $(document).ready(function () {
            $('#year').pickadate({
                selectYears: 80,
                min:new Date(1940,0,1),
                max: new Date('today'),
                selectMonths: true,
                format: 'yyyy',
            });
        })
    </script>
@endsection
