
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

import ToggleButton from 'vue-js-toggle-button'
import axios from 'axios'

Vue.use(ToggleButton);
Vue.use(axios);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#wrapper',
    data(){
        return {
            appUrl: '',
            toggle_data: ''
        }
    },
    mounted(){
      this.appUrl= $('meta[name="appUrl"]').attr('content');
    },
    methods: {
        async updateCustomerType(event,user_id)
        {
            var _this = this;
            console.dir(_this.toggle_data);
            event.value = _this.toggle_data;
            console.dir(event);
            if(confirm("Esta seguro que desea cambiar")) {
                await axios.post(_this.appUrl + '/profile/updateCustomerType', {user_id: user_id})
                    .then((response) => {
                        // (response.data.user.type === 'passenger') ? event.value = true : event.value = false;
                        (response.data.user.type === 'passenger') ? _this.toggle_data = true : _this.toggle_data = false;
                        toastr.success('Actualizado correctamente', 'OK');
                    })
                    .catch(error => {
                        console.log(error);
                    })
            }

            event.value = _this.toggle_data;

        }
    }
});
