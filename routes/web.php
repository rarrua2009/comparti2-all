<?php

Route::redirect('/', '/page');

Auth::routes(['register' => false]);
Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::post('/login/admin', 'Auth\LoginController@adminLogin')->name('admin.login.post');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth:admin']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');
    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');
    Route::resource('products', 'ProductsController');
    Route::get('settings/index','ConfigController@index')->name('settings.index');
    Route::put('settings/update/{config_id}','ConfigController@update')->name('settings.update');
});

Route::group(['prefix' => 'page','as'=>'page.','namespace' => 'Frontend'], function () {
    Route::redirect('/', 'page/landing');
    Route::get('landing','HomeController@index')->name('landing');
    Route::get('/login/customers', 'LoginController@showLoginForm')->name('customer.login');
    Route::post('/login/customers', 'LoginController@customerLogin')->name('customer.login.post');

    Route::get('/register/customer', 'RegisterController@showRegistrationForm')->name('customer.register');
    Route::post('/register/customer', 'RegisterController@register')->name('customer.register.post');

    Route::get('profile/account/{user_id}','ProfileController@index')->name('profile.index');
    Route::put('profile/update/{user_id}','ProfileController@update')->name('profile.update');
    Route::post('profile/update/password','ProfileController@updatePassword')->name('profile.update.password');
    Route::post('profile/updateCustomerType','ProfileController@updateCustomerType')->name('profile.update.type');

    Route::get('profile/travels/reserved','ProfileController@travelsReserved')->name('profile.travels.reserved');
    Route::get('profile/travels/created','ProfileController@travelsCreated')->name('profile.travels.created');
    Route::get('profile/travels/created/edit/{travel_id}','ProfileController@travelsCreatedEdit')->name('profile.travels.created.edit');
    Route::put('profile/travels/created/update/{travel_id}','ProfileController@travelsCreatedUpdate')->name('profile.travels.created.update');
    Route::get('profile/reviews','ProfileController@reviews')->name('profile.reviews');
    Route::get('profile/notifications','ProfileController@notifications')->name('profile.notifications');

    Route::get('profile/cars/', 'ProfileController@carsIndex')->name('profile.cars.index');
    Route::get('profile/cars/edit', 'ProfileController@carsEdit')->name('profile.cars.edit');
    Route::put('profile/cars/update/{card_id}', 'ProfileController@carsUpdate')->name('profile.cars.update');
    Route::get('profile/cars/create', 'ProfileController@carsCreate')->name('profile.cars.create');
    Route::post('profile/cars/store', 'ProfileController@carsStore')->name('profile.cars.store');

    Route::get('profile/payments','ProfileController@payments')->name('profile.payments');
    Route::get('travel/find/','TravelController@find')->name('travel.find');
    Route::get('travel/find/search','TravelController@findSearch')->name('travel.find.search');
    Route::get('travel/details/{travel_id}','TravelController@details')->name('travel.details');
    Route::get('travel/reserve/{path_id}','TravelController@reserveTravel')->name('travel.reserve');

    Route::get('travel/create/','TravelController@create')->name('travel.create');
    Route::post('travel/create/store','TravelController@createTravel')->name('travel.store');

});
