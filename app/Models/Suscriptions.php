<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Suscriptions extends Model
{
    protected $table = 'suscriptions';

    protected $fillable = ['path_id','user_id'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function path()
    {
        return $this->belongsTo(Paths::class,'path_id');
    }
}
