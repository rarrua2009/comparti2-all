<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 'ratings';

    protected $fillable = ['description','rating','user_id'];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
