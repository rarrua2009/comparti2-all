<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Travels extends Model
{
    protected $table = 'travels';

    protected $fillable = [
        'begin_location_lat',
        'begin_location_long',
        'end_location_lat',
        'end_location_long',
        'stop_lat',
        'stop_long',
        'begin_address',
        'end_address',
        'stop_address',
        'departure_date_time',
        'return_date_time',
        'user_id',
        'path_id',
        'state',
        'price',
        'price_to_stop',
        'car_id'
    ];

    protected $dates = ['departure_date_time'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function paths()
    {
        return $this->belongsTo(Paths::class,'path_id');
    }

    public function car()
    {
        return $this->belongsTo(Cars::class,'car_id');
    }

    public function passengers()
    {
        return $this->hasMany(Passengers::class,'travel_id');
    }

    public function getPriceAttribute()
    {
        return number_format($this->attributes['price'],'0','','.');
    }

    public function getPriceToStopAttribute()
    {
        return number_format($this->attributes['price_to_stop'],'0','','.');
    }
}
