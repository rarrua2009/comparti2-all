<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Passengers extends Model
{
   protected $table = 'passengers';

   protected $fillable = ['user_id','travel_id'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function travel()
    {
        return $this->belongsTo(Travels::class,'travel_id');
    }
}
