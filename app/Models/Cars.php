<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    protected $table = 'cars';

    protected $fillable = ['name','brand','model','year','active','user_id',
        'plate','license_front','license_back','green_license_front','green_license_back'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function travels()
    {
        return $this->hasMany(Travels::class,'car_id');
    }
}
