<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paths extends Model
{
    protected $table = 'paths';

    protected $fillable = ['name','origin','destination','round_trip'];

    public function suscriptions()
    {
        return $this->hasMany(Suscriptions::class,'path_id');
    }

    public function travel()
    {
        return $this->hasOne(Travels::class,'path_id');
    }
}
