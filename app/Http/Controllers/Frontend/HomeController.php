<?php


namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Repositories\CategoriesRepository;
use Illuminate\Contracts\Support\Renderable;
use Auth, Cart;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('frontend.landing.index');
    }
}
