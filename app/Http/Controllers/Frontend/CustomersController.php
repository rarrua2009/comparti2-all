<?php


namespace App\Http\Controllers\Frontend;


use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomersAddresRepository;
use App\Repositories\CustomersRepository;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth, Log;

class CustomersController extends Controller
{

    protected $customer;

    protected $customerRepository;

    protected $customerAddressRepository;

    public function __construct(CustomersRepository $customerRepository, CustomersAddresRepository $customerAddressRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->customerAddressRepository = $customerAddressRepository;
        $this->middleware('auth:customer');
    }

    public function profile($customer_id)
    {

        $customer = $this->customerRepository->find($customer_id);
        return view('frontend.customers.account.profile', compact('customer'));
    }

    public function profileUpdate(UpdateCustomerRequest $request, $customer_id)
    {
        if ($request->has('password'))
        {
            $request->password = \Hash::make($request->password);
        }
        $this->customerRepository->update($request->all(),$customer_id);
        toastSuccess('Pefil actualizado','Actualizado');
        return redirect()->back();
    }

    public function orders($customer_id)
    {
        $customer = $this->customerRepository->find($customer_id);
        return view('frontend.customers.account.orders',compact('customer'));
    }

    public function wishlist($customer_id)
    {
        $customer = $this->customerRepository->find($customer_id);
        return view('frontend.customers.account.wishlist',compact('customer'));
    }

    public function address($customer_id)
    {
        $customer = $this->customerRepository->find($customer_id);
        return view('frontend.customers.account.address.index',compact('customer'));
    }

    public function addressEdit($address_id)
    {
        $address = $this->customerAddressRepository->find($address_id);
        return view('frontend.customers.account.address.edit',compact('address'));
    }

    public function addressUpdate(Request $request,$address_id)
    {
        $this->customerAddressRepository->update($request->all(),$address_id);
        toastr()->success('Direccion actualizada correctamente','OK!!');
        return redirect()->back();
    }

    public function addressAdd($customer_id)
    {
        $customer = $this->customerRepository->find($customer_id);
        return view('frontend.customers.account.address.add',compact('customer'));
    }

    public function addressStore(Request $request)
    {
        $this->customer = auth()->guard('customer')->user();
        $requestData = null;
        if ($request->has('default_address'))
        {
            $requestData = $request->all();
            $requestData['default_address'] = true;

            if ($this->customer->addresses->count() >= 1) {
                foreach ($this->customer->addresses as $address){
                    $data['default_address'] = false;
                    $this->customerAddressRepository->update($data,$address->id);
                }
            }
        }

        $this->customerAddressRepository->create($requestData);
        toastr()->success('Direccion agregada', 'OK!!');
        return redirect()->route('page.customer.address',$request->customer_id);
    }

    public function addressDelete($address_id)
    {
        $this->customerAddressRepository->delete($address_id);
        toastr()->success('Direccion eliminada correctamente','Eliminado');
        return redirect()->back();
    }

}