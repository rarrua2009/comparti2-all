<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Passengers;
use App\Models\Paths;
use App\Models\Travels;
use App\Models\Suscriptions;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class TravelController extends Controller
{
    public function __construct()
    {
        $this->middleware('cliente')->except(['create','findSearch','find']);
    }

    public function validateTravel(array $request)
    {
        return \Validator::make($request,[
            'address_address' => 'required',
            'end_addres' => 'required',
            'fecha_salida_submit'=>'required',
            'hora_salida_submit' => 'required',
            'card_id' => 'required',
            'seat' => 'required'
        ],[
            'address_address.required' =>'Lugar de salida es requerido',
            'end_addres.required' => 'Lugar de llegada es requerido',
            'fecha_salida_submit.required' => 'Fecha de salida es requerido',
            'hora_salida_submit.required' => 'Hora de salido es requerido',
            'card_id.required' => 'Vehiculo para el viaje es requerido',
            'seat.required' => 'Asientos es requerido',

        ]);
    }

    public function find()
    {
        return view('frontend.travels.find');
    }

    public function findSearch()
    {
        $paths = new Paths();
        $search = $paths->newQuery();

        $search->whereHas('travel', function ($q){
            $q->where('state','=','active');
        });

        if(request()->has('search_travel') && request()->search_travel_origin !== null)
        {
            $search_word = explode(',',request()->search_travel);
            $search->where('origin','like','%'. $search_word[0].'%')
                ->orWhere('destination','like','%'. $search_word[0].'%');
        }

        if (request()->has('search_travel_origin') && request()->search_travel_origin !== null)
        {
            $search_word = explode(',',request()->search_travel_origin);
            $search->where('origin','like','%'. $search_word[0].'%');
        }

        if (request()->has('search_travel_destiny') && request()->search_travel_destiny !== null)
        {
            $search_word = explode(',',request()->search_travel_destiny);
            $search->where('destination','like','%'. $search_word[0].'%');
        }

        if (request()->has('date_of_travel') && request()->date_of_travel !== null) {
            $date_carbon = Carbon::createFromFormat('Y-m-d', request()->date_of_travel)->day;
            $search->whereHas('travel', function (Builder $q) use ($date_carbon) {
                $q->whereDay('departure_date_time', $date_carbon);
            });
        }

        $search = $search->paginate(10);
        return view('frontend.travels.search',compact('search'));
    }

    public function create()
    {
        $cars = (\Auth::check()) ? \Auth::user()->cars()->get() : null;
        return view('frontend.travels.create',compact('cars'));
    }

    public function createTravel(Request $request)
    {
        //dd($request->all());

        $validator = $this->validateTravel($request->all());

        if ($validator->fails())
        {
            toastr()->error('Hubo un error en los datos, favor verificar','Error');
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $path = Paths::create([
            'name' => $request->address_address . ' - '. $request->end_addres,
            'origin'=>$request->address_address,
            'destination'=> $request->end_addres,
            'round_trip'=> ($request->has('round_trip') ? true : false)
        ]);

        $departure_date_time = Carbon::createFromFormat('Y-m-d H:i', $request->fecha_salida_submit .' '. $request->hora_salida_submit);

        $travel = Travels::create([
            'begin_location_lat' => $request->address_latitude,
            'begin_location_long'=>$request->address_longitude,
            'end_location_lat'=>$request->end_address_latitude,
            'end_location_long'=>$request->end_addres_longitude,
            'stop_lat'=>($request->has('stop_lat') ?  $request->stop_lat : ''),
            'stop_long'=>($request->has('stop_long') ? $request->stop_long : ''),
            'begin_address'=>$request->address_address,
            'end_address'=>$request->end_addres,
            'stop_address'=>($request->has('stop_address') ? $request->stop_address : ''),
            'price_to_stop'=>($request->has('price_to_stop') ? $request->price_to_stop : ''),
            'departure_date_time'=>$departure_date_time,
            'user_id'=>\Auth::id(),
            'path_id'=>$path->id,
            'state'=>'active',
            'price'=>$request->price,
            'car_id'=>$request->card_id
        ]);

        toastr()->success('Viaje publicado correctamente','OK');
        return redirect()->back();


    }

    public function reserveTravel($path_id)
    {
        Suscriptions::create([
            'path_id' => $path_id,
            'user_id' => \Auth::id()
        ]);

        $travel = Travels::where('path_id',$path_id)->first();
        Passengers::create([
            'user_id'=>\Auth::id(),
            'travel_id' =>$travel->id
        ]);

        $travel->state = 'reserved';
        $travel->save();

        toastr()->success('Viaje reservado','OK');
        return redirect()->route('page.profile.travels.reserved');
    }

    public function details($travel_id)
    {
        $travel = Travels::find($travel_id);
        return view('frontend.travels.details',compact('travel'));
    }

}
