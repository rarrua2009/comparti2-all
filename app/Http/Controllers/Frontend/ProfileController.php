<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Cars;
use App\Models\Notifications;
use App\Models\Travels;
use App\Models\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class ProfileController extends Controller
{
    public $customer;

    public $marcas = ['nissan','toyota','volvo','renault','kia','hyundai','jeep','mazda','mercedes',
    'geely','cherry','baic'];

    public function __construct()
    {
        $this->middleware('cliente');
        $this->customer = \Auth::user();
    }

    public function index($user_id)
    {
        $this->customer = User::find($user_id);

        return view('frontend.profile.index',with(['customer'=>$this->customer]));
    }

    public function update(Request $request, $id)
    {
        $validator =  Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => ['required',Rule::unique('users')->ignore(\Auth::id()),],
        ],[ 'firstname.required'=> 'El nombre es requerido',
            'lastname.required'=> 'El apellido es requerido',
            'email.unique'=> 'El email existe, favor cambiarlo',
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $user = User::find($id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        //$user->type = $request->type;
        $user->sex = $request->sex;
        $user->date_birth = $request->date_birth_submit;

        $user->save();
        toastr()->success('Perfil actualizado', 'Ok!');
        return redirect()->back();
    }

    public function updatePassword(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'password' => 'required|confirmed',
        ], [
            'password.required'=> 'La contraseña nueva es requerida',
            'password.confirmed'=> 'La contraseña no es la misma que la contraseña de confirmacion',
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $user = User::find(\Auth::id());
        $user->password = \Hash::make($request->password);
        $user->save();

        toastr()->success('Contraseña actualizada correctamente', 'Ok!');
        return redirect()->back();
    }

    public function updateCustomerType(Request $request)
    {
        $user = User::find($request->user_id);
        ($user->type == 'passenger') ? $user->type = 'traveler' : $user->type = 'passenger';
        $user->save();

        return response()->json(['status'=>'ok','user'=>$user]);
    }

    public function travelsReserved()
    {
        $customer = $this->customer;

        $travels = Travels::whereHas('passengers', function ($query) {
            $query->where('user_id', \Auth::id());
        })->get();
        return view('frontend.profile.viajes.reservados',compact('travels','customer'));
    }

    public function travelsCreated()
    {
        $customer = $this->customer;
        $travels = Travels::where('user_id',\Auth::id())->get();
        return view('frontend.profile.viajes.publicados',compact('travels','customer'));
    }

    public function travelsCreatedEdit($travel_id)
    {
        $travel = Travels::find($travel_id);
        return view('frontend.profile.viajes.editar',compact('travel'));
    }

    public function reviews()
    {

    }

    public function notifications()
    {
        $notifications = Notifications::where('user_id',\Auth::id())->get();
        return view('frontend.profile.notificaciones',compact('notifications','customers'));
    }

    public function carsIndex()
    {
        $cars = Cars::where('user_id',\Auth::id())->get();
        return view('frontend.profile.cars.index',compact('cars'));
    }

    public function carsCreate()
    {
        $marcas = $this->marcas;
        return view('frontend.profile.cars.create',compact('marcas'));
    }

    public function carsStore(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'brand' =>'required',
            'model' =>'required',
            'year' =>'required',
            'license_front'=>'required',
            'license_back'=>'required',
            'plate'=>'required',
            'green_license_front'=>'required',
            'green_license_back'=>'required'
        ], [
            'name.required' => 'El Nombre es obligatorio',
            'brand.required' => 'La Marca es obligatorio',
            'model.required' => 'EL Modelo es obligatorio',
            'year.required' => 'El Año es obligatorio',
            'license_front.required' => 'Imagen frontal de la Licencia de conducir es requerido',
            'license_back.required' => 'Imagen trasera de la Licencia de conducir es requerido',
            'green_license_front.required' => 'Imagen frontral de la Cedula verde es requerido',
            'green_license_back.required' => 'Imagen Trasera de la Cedula verde es requerido',
            'plate.required' => 'Licencia de conducir es requerido',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $path = 'cars/'.\Auth::id();
        $license_front = time().'.'.$request->file('license_front')->extension();
        $license_back = time().'.'.$request->file('license_back')->extension();
        $plate = time().'.'.$request->file('plate')->extension();
        $green_license_front = time().'.'.$request->file('green_license_front')->extension();
        $green_license_back = time().'.'.$request->file('green_license_back')->extension();

        $request->file('license_front')->move(public_path($path),$license_front);
        $request->file('license_back')->move(public_path($path),$license_back);
        $request->file('plate')->move(public_path($path),$plate);
        $request->file('green_license_front')->move(public_path($path),$green_license_front);
        $request->file('green_license_back')->move(public_path($path),$green_license_back);

        Cars::create([
            'name' => $request->name,
            'brand' => $request->brand,
            'model' => $request->model,
            'year' => $request->year,
            'user_id' => \Auth::id(),
            'plate' => $path .'/'.$plate,
            'license_front' => $path .'/'.$license_front,
            'license_back' => $path .'/'.$license_back,
            'green_license_front' => $path .'/'.$green_license_front,
            'green_license_back' => $path .'/'.$green_license_back,
        ]);

        toastr()->success('Auto agregado correctamente','OK');
        return redirect()->route('page.profile.cars.index');
    }

    public function carsEdit($car_id)
    {
        $car = Cars::find($car_id);
        return view('frontend.profile.cars.edit',compact('car'));
    }

    public function carsUpdate(Request $request, $car_id)
    {

    }
}
