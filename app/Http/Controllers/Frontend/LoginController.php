<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\View\View;
use Cart;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/page';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        //$this->middleware('guest:customer')->except('logout');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  Request  $request
     * @return Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }


    /**
     * Show the application's login form.
     *
     * @return View
     */
    public function showLoginForm()
    {
        return view('auth.login_customers');
    }

    public function customerLogin(Request $request)
    {
        $validator = $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);


        if ($this->guard()->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            toastSuccess('Bienvenido de nuevo','OK!!');
            toastr()->success('Bienvenido de nuevo','Ok!!');
            return redirect()->intended('/page/landing');
        }

        return back()->withInput($request->only('email', 'remember'));
    }



//    /**
//     * Get the guard to be used during authentication.
//     *
//     * @return StatefulGuard
//     */
//    protected function guard()
//    {
//        return Auth::guard('customer');
//    }
}
