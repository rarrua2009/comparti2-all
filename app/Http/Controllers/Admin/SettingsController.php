<?php

namespace App\Http\Admin\Controllers;

use App\Models\Settings;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $configs = Settings::all();
        return view('admin.settings.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Settings $setting
     * @return RedirectResponse
     */
    public function update(Request $request, Settings $setting)
    {
        $setting->precio_por_viaje = $request->precio_por_viaje;
        $setting->save();
        toastSuccess('Configuracion actualizada correctamente','OK');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Config $config
     * @return \Illuminate\Http\Response
     */
    public function destroy(Config $config)
    {
        //
    }
}
